from shutil import copyfile
from orm import *
from dropbox_helper import *
import util


class FileObject:

    # create a new instance of the fileobject. To create we need the filename. The version
    # is optional. If version is specified, we use it in the remote path, else a new version
    # is generated
    def __init__(self, g, filename, version = None, localpath = None):
        self.logger = g['logger']
        self.drp = g['drp']
        self.global_vars = g
        self.filename = filename
        self.local_path = None
        self.remote_path = None


        self.logger.debug('id of drp is %d and is it an instance of DropboxClient? %s' %
                          (id(self.drp), isinstance(self.drp, DropboxClient)))

        if version is None:
            self.version = util.get_version(self.logger)
        else:
                self.version = version


        self.remote_path = self.global_vars['dropbox_folder'] + '/' + \
                            self.global_vars['source'] + '.' + self.version

        if localpath is None:
            self.local_path = self.global_vars['local_temp_dir'] + '/' + \
                          self.global_vars['source'] + '.' + self.version
        else:
            self.local_path = localpath

        self.logger.info("Started file instance for file %s/%s with version %s"
                         %(self.filename, self.remote_path, self.version))


    def __str__(self):
        return self.filename + ' ' + self.version

    def download(self):
        self.logger.info('Trying to download %s from %s into %s' %(self.filename, self.remote_path, self.local_path))
        r = self.drp.download_file(local_file= self.local_path, remote_file= self.remote_path)
        if r == 0:
            self.logger.info('Found remote file %s' %self.remote_path)
            return True
        else:
            self.logger.info('Could not find remote file %s' %self.remote_path)
            return False

    def backup(self):
        cmd = 'shutil.copyfile'
        target = 'dropbox'
        commit_comment = 'commit comment'
        orm = self.global_vars['orm']
        b = orm.add_file(self.filename, self.version, cmd, target, commit_comment)

        # TODO : change dropbox exceptions so that we catch these exceptions in dropbox_helper and raise
        # TODO: other appropriate exceptions
        try:
            self.logger.debug('trying to upload file %s' % (self.filename))
            orm = self.global_vars['orm']
            orm.upload_database_file()
        except dropbox.exceptions.ApiError as err:
            self.logger.error("Failed to upload file. Error %s" % err)

        try:
            self.logger.debug('Backing up file %s to dropbox:: %s' %(self.filename, self.remote_path))
            #drp = self.global_vars['drp']
            self.logger.debug('id of drp is %d and is it an instance of DropboxClient? %s' %
                              (id(self.drp), isinstance(self.drp, DropboxClient)))
            self.drp.upload_file(self.filename, self.remote_path)
        except dropbox.exceptions.ApiError as err:
            self.logger.error("Failed to database file. Error %s" % err)
            b.delete()
