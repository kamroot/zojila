from peewee import *
from datetime import datetime


# TODO - this needs to be read from a conf variable. Right now, I don't know how to
# structure the project to make this be read from the config variable
DB_FILE = '/tmp/backup.db'
db = SqliteDatabase(DB_FILE)
class Backup(Model):
    filename = CharField()
    version = CharField() # version of this file or database
    target = CharField() # what are we backing up? Can be FILE or DATABASE
    comment = CharField()
    command = CharField() # any command used to generate the backup
    database = CharField()
    author = CharField()
    hostname = CharField()
    time = DateTimeField(default = datetime.now)

    class Meta:
        database = db

class Orm():
    def download_datatase_file(self):
        self.logger.info("Downloading database locally from %s to %s" % (self.remote_db_file, self.local_db_file))
        r = self.drp.download_file(self.local_db_file, self.remote_db_file)
        if r != 0:
            self.logger.info('Remote database does not exist. It will need to be created')
        return r

    # function to upload the local database file
    def upload_database_file(self):
        self.logger.info("Uploading local database file from %s to %s" % (self.local_db_file, self.remote_db_file))
        self.drp.upload_file(self.local_db_file, self.remote_db_file)

    def add_file(self, name, version, cmd, target, commit_comment):
        self.logger.info('Inside add file')
        #version = util.get_version(self.logger, self.global_vars['source'])
        d = 'NA'

        b = Backup(filename = name, version = version, command=cmd, target=target, comment=commit_comment, database=d)
        b.time = datetime.now()
        b.author = self.global_vars['author']
        b.hostname = 'NA'
        b.save()
        return b

    def list(self):
        self.logger.info("Inside ORM List")
        count = 0
        lst = Backup.select()
        for b in lst:
            count = count + 1
            #print (b.version, b.target, b.comment, b.command)
        self.logger.info("Returned %d records" %count)
        return lst

    def __init__(self, g):
        self.logger = g['logger']
        self.global_vars = g
        self.drp = g['drp']
        # TODO fix this
        self.local_db_file = DB_FILE
        self.remote_db_file = g['dropbox_folder'] + '/' + g['sqllite_db_name']
        self.drp = g['drp']
        self.logger.info(
            'Started orm instance. local db %s and remote db path %s' % (self.local_db_file, self.remote_db_file))

        # now get the database file from remote location. If it does not exist, create it.
        self.download_datatase_file()

        db.connect()
        db.create_tables([Backup], safe=True)




