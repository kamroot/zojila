#!/usr/bin/python3

# copyright Flexidot
# Basic file to backup a wordpress database and store what we backuped up, along with
# the time and comment in a simple sqllite database
# ASSUMPTION : There is a directory at the top level where all the sources after_widget
# stored that is called backups.
# If that directory does not exist, then the script fails


from fileobject import *
from util import *
import sys, fire
from orm import *


def backup(config_file = None):
    g = initial_setup()
    logger = g['logger']
    logger.info("inside backup")
    fo = FileObject(g, filename = g['source'])
    fo.backup()
    final_cleanup(g)

def list_backups(loglvl = logging.ERROR):
    g = initial_setup(loglvl)
    logger = g['logger']
    logger.info("inside list backups")
    orm = g['orm']
    lst = orm.list()
    for b in lst:
        print (b.filename, b.author, b.time, b.version, b.target, b.comment, b.command)
    final_cleanup(g)

def verify(loglvl = logging.ERROR):
    g = initial_setup(loglvl)
    logger = g['logger']
    logger.info("inside verify")
    lst = g['orm'].list()
    total = 0
    error = 0
    for b in lst:
        total = total + 1
        logger.info('trying to verify %s' %b.version)
        f = FileObject(g, filename = b.filename, version = b.version)
        if f.download() == False:
            error = error + 1
            logger.error('File %s does not exist on dropbox' %f.remote_path)
    final_cleanup(g)
    print ('Found a total of %d file in db. Of those %d do not exist' %(total, error))

def prune(config_file = None):
    g = initial_setup()
    logger = g['logger']
    logger.info("inside prune")

    final_cleanup(g)


def trim(config_file = None):
    g = initial_setup()
    logger = g['logger']
    logger.info("inside trim")

    final_cleanup(g)


def download(version, name, localpath = None, loglvl = logging.ERROR):
    g = initial_setup(loglvl)
    logger = g['logger']
    logger.info("inside download")

    fo = FileObject(g, filename=name, version=version, localpath=localpath)
    fo.download()

    final_cleanup(g)

   # f = FileObject(g, filename=b.filename, version=b.version)
   # if f.download() == False:


fire.Fire({
      'backup': backup,
      'list': list_backups,
      'verify': verify,
      'prune': prune,
      'trim': trim,
      'download': download
  })

sys.exit(0)
#file = open('dropbox.token', 'r')
#access_token = file.read()

# cmd="docker run -it --link mysql:mysql --rm mysql sh -c 'exec mysqldump \
#    -h\"$MYSQL_PORT_3306_TCP_ADDR\" -P\"$MYSQL_PORT_3306_TCP_PORT\" \
#    -uroot -p\"$MYSQL_ENV_MYSQL_ROOT_PASSWORD\" " + database_to_backup + "' > " + name
