import dropbox


class DropboxClient:
    def __init__(self, logger, global_vars):
        self.logger = logger
        self.global_vars = global_vars

        logger.debug('dropbox token is in file %s' % global_vars['dropbox_token_file'])
        file = open(global_vars['dropbox_token_file'], 'r')
        self.access_token = file.read()
        logger.info("Token is %s" %self.access_token)
        self.dbx = dropbox.Dropbox(self.access_token)

    def upload_file(self, file_from, file_to):
        """upload a file to Dropbox using API v2
		"""
        self.logger.info('Trying to upload file %s to %s' % (file_from, file_to))
        with open(file_from, 'rb') as f:
            self.dbx.files_upload(f.read(), file_to, mode=dropbox.files.WriteMode.overwrite)

    def download_file(self, local_file, remote_file):
        """Download a file from Dropbox using API v2
		"""
        self.logger.info('Trying to downloadfile %s into local %s' % (remote_file, local_file))
        try:
            self.dbx.files_download_to_file(local_file, remote_file)
        except dropbox.exceptions.ApiError as err:
            if isinstance(err, dropbox.files.DownloadError) == True:
                self.logger.debug('File %s does not exist' % remote_file)
                return -1
            else:
                self.logger.debug('Could not download %s. Unknown error %s' % (remote_file, err))
                return -1
        except dropbox.exceptions.AuthError as err:
            self.logger.error("Failed authentication. Error %s" %err)
            return -1
        self.logger.info('Successfully download %s into %s' % (remote_file, local_file))
        return 0
