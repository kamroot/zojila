import os, logging, json
from random import randint
import socket
from shutil import rmtree
from orm import *
from datetime import datetime
from dropbox_helper import *


def get_hostname():
    return socket.gethostname()

def setup_logging(loglvl):
	logger = logging.getLogger('zojila')
	logger.setLevel(logging.DEBUG)
	# create file handler which logs even debug messages
	fh = logging.FileHandler('deploy.log')
	fh.setLevel(logging.DEBUG)
	# create console handler with a higher log level
	ch = logging.StreamHandler()
	ch.setLevel(loglvl)
	# create formatter and add it to the handlers
	formatter = logging.Formatter('%(asctime)s,%(msecs)d %(levelname)-8s [%(name)s::%(filename)s:%(lineno)d] %(message)s')
	fh.setFormatter(formatter)
	ch.setFormatter(formatter)
	# add the handlers to the logger
	logger.addHandler(fh)
	logger.addHandler(ch)
	return logger


def load_globals(logger, f):
	try:
		json_fd = open(f).read()
	except FileNotFoundError as e:
		logger.error('ERROR: could not load project globals json file %s.' %JSON_FILE)
		exit(-1)

	try:
		project_globals = json.loads(json_fd)
	except json.decoder.JSONDecodeError as e:
		logger.error('ERROR: unable to parse json file %s. %s.' %(file, e))
		exit (-1)

	return project_globals

# check if local temp dir exists
def check_local_dir(logger, g):
	logger.info("Checking if local temp dir %s exists" %g['local_temp_dir'])
	if os.path.isdir(g['local_temp_dir']) == False:
		logger.info('Path %s does not exist. going to create it' %g['local_temp_dir'])
		os.makedirs(g['local_temp_dir'])

	return

# TODO need to change this function so that the version returned is the same for everytime it is called
# within the same program. Right now, it is called twice and both times it will return a different version
def get_version(logger):
	today = datetime.now()
	d = today.strftime('%d_%b_%Y')
	r = randint(1000, 100000)

	version = today.strftime("%Y_%m_%d") + '___' + str(r)
	logger.info("Returning version %s" %version)
	return version

# this is the funtion that makes sure all the conditions needed for running zojila are satisfied
def initial_setup(loglvl = logging.INFO):
    logger = setup_logging(loglvl)
    g = load_globals(logger, 'config.json')
    logger.info("Initial setup with file config.json")
    drp = DropboxClient(logger, g)
    g['logger'] = logger
    g['drp'] = drp
    logger.debug('id of drp is %d and is it an instance of DropboxClient? %s' %(id(drp), isinstance(drp, DropboxClient)))
    orm = Orm(g)
    g['orm'] = orm


    check_local_dir(logger, g)
    return g


def final_cleanup(g):
    logger = g['logger']
    ## finally, remove the local backup directory
    rmtree(g['local_temp_dir'])
    logger.info('Removed %s' %g['local_temp_dir'])

    os.remove('/tmp/backup.db')
    logger.info('Finishing program')
